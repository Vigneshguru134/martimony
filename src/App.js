import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Login from "./Component/Login";
import Signup from "./Component/Signup";
import Addnew from "./Component/Addnew";
import Home from "./Component/Home"
import popover from "./Component/Popover"
//import firebase from "./Firebase";
import "./App.css";

export default class App extends Component {
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Login} />
          <Route path="/Signup" component={Signup} />
          <Route path="/Addnew" component={Addnew} />
          <Route path="/Home" component={Home} />
          <Route path="/popover" component={popover} />
        </Switch>
      </Router>
    );
  }
}
