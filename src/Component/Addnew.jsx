import React, { Component } from "react";
import {
  Input,
  Label,
  Form,
  Button,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  ButtonDropdown,
  Spinner,
} from "reactstrap";
import Tableoutput from "./Tableoutput";
import firebase from "../Firebase";
import { v4 as uuidv4 } from "uuid";
import { withSnackbar } from "react-simple-snackbar";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

class Addnew extends Component {
  state = {
    Name: "",
    age: "",
    profession: "",
    Location: "",
    fname: "",
    mname: "",
    nakshatra: "",
    lagnam: "",
    thidhi: "",
    birthplace: "",
    Deergasam: "",
    Ayanamsa: "",
    neramandalam: "",
    kulam: "",
    gothiram: "",
    color: "",
    degree: "",
    brosis: "",
    mothertTongue: "",
    inam: "",
    height: "",
    yogam: "",
    sunrise: "",
    sunset: "",
    karnam: "",
    address: "",
    phnnum: "",
    tamilyear: "",
    natchathirahorai: "",
    udayadinaligai: "",
    ayanam: "",
    atchamsum: "",
    rasi: null,
    rasiathipathi: null,
    tamilmonth: null,
    tamildate: null,
    startDate: new Date(),
    dropdownOpen1: false,
    dropdownOpen2: false,
    dropdownOpen3: false,
    dropdownOpen4: false,
    dropdownOpen5: false,
    dropdownOpen6: false,
    gender: null,
    isLoading: false,
    newinputs1: [
      ["A", "B", "C", "D"],
      ["E", "F", "G", "H"],
      ["I", "J", "K", "L"],
    ],
    newinputs2: [
      ["A", "B", "C", "D"],
      ["E", "F", "G", "H"],
      ["I", "J", "K", "L"],
    ],
  };

  handleName = (event) => {
    this.setState({ Name: event.target.value });
    const arr = [];
    arr.concat();
  };
  handleAge = (event) => {
    this.setState({ age: event.target.value });
  };
  birthplace = (event) => {
    this.setState({ birthplace: event.target.value });
  };
  handleProfession = (event) => {
    this.setState({ profession: event.target.value });
  };
  handlefields1 = (event, subinput) => {
    this.setState({ [`input${subinput}`]: event.target.value });
  };
  handlefields2 = (event, subinput2) => {
    this.setState({ [`input2${subinput2}`]: event.target.value });
  };
  handleLocation = (event) => {
    this.setState({ Location: event.target.value });
  };
  handlefname = (event) => {
    this.setState({ fname: event.target.value });
  };
  handlemname = (event) => {
    this.setState({ mname: event.target.value });
  };
  handlenakshatra = (event) => {
    this.setState({ nakshatra: event.target.value });
  };
  Ayanamsa = (event) => {
    this.setState({ Ayanamsa: event.target.value });
  };
  gothiram = (event) => {
    this.setState({ gothiram: event.target.value });
  };
  handlelagnam = (event) => {
    this.setState({ lagnam: event.target.value });
  };
  neramandalam = (event) => {
    this.setState({ neramandalam: event.target.value });
  };
  natchathirahorai = (event) => {
    this.setState({ natchathirahorai: event.target.value });
  };
  kulam = (event) => {
    this.setState({ kulam: event.target.value });
  };
  ayanam = (event) => {
    this.setState({ ayanam: event.target.value });
  };
  atchamsum = (event) => {
    this.setState({ atchamsum: event.target.value });
  };
  handledegree = (event) => {
    this.setState({ degree: event.target.value });
  };
  handlemotherTongue = (event) => {
    this.setState({ mothertTongue: event.target.value });
  };
  Deergasam = (event) => {
    this.setState({ Deergasam: event.target.value });
  };
  tamilyear = (event) => {
    this.setState({ tamilyear: event.target.value });
  };
  udayadinaligai = (event) => {
    this.setState({
      udayadinaligai: event.target.value,
    });
  };
  exit = () => {
    this.props.history.push("/home");
  };
  color = (event) => {
    this.setState({ color: event.target.value });
  };
  phnnum = (event) => {
    this.setState({ phnnum: event.target.value });
  };
  address = (event) => {
    this.setState({ address: event.target.value });
  };
  handleDate = (date) => {
    console.log(this.state.startDate);

    this.setState({ startDate: date });
    //this.setState({ date: this.state })

    //console.log('date', Object.values(this.state.startDate))
    //console.log(this.state.startDate.getDate() - this.state.startDate.getDay() - this.state.startDate.getFullYear)
  };
  karnam = (event) => {
    this.setState({ karnam: event.target.value });
  };
  inam = (event) => {
    this.setState({ inam: event.target.value });
  };

  thidhi = (event) => {
    this.setState({ thidhi: event.target.value });
  };
  gendertoggle = () => {
    this.setState({
      dropdownOpen1: !this.state.dropdownOpen1,
    });
  };
  tamilmonth = () => {
    this.setState({
      dropdownOpen4: !this.state.dropdownOpen4,
    });
  };
  tamilyear = () => {
    this.setState({
      dropdownOpen6: !this.state.dropdownOpen6,
    });
  };
  tamildate = () => {
    this.setState({
      dropdownOpen5: !this.state.dropdownOpen5,
    });
  };

  rasitoggle = () => {
    this.setState({
      dropdownOpen2: !this.state.dropdownOpen2,
    });
  };

  yogam = (event) => {
    this.setState({ yogam: event.target.value });
  };
  sunrise = (event) => {
    this.setState({ sunrise: event.target.value });
  };
  sunset = (event) => {
    this.setState({ sunset: event.target.value });
  };

  rasiathipatitoggle = () => {
    this.setState({
      dropdownOpen3: !this.state.dropdownOpen3,
    });
  };
  handlegenderChange = (event) => {
    // console.log(this.state.gender)
    // console.log('log2', event.target.innerText)
    this.setState({
      gender: event.target.innerText,
    });
  };
  handletamilmonth = (event) => {
    // console.log(this.state.gender)
    // console.log('log2', event.target.innerText)
    this.setState({
      tamilmonth: event.target.innerText,
    });
  };
  handletamilyear = (event) => {
    // console.log(this.state.gender)
    // console.log('log2', event.target.innerText)
    this.setState({
      tamilyear: event.target.innerText,
    });
  };
  handletamildate = (event) => {
    // console.log(this.state.gender)
    // console.log('log2', event.target.innerText)
    this.setState({
      tamildate: event.target.innerText,
    });
  };
  handlerasiChange = (event) => {
    this.setState({
      rasi: event.target.innerText,
    });
  };
  handlerasiathipathiChange = (event) => {
    this.setState({
      rasiathipathi: event.target.innerText,
    });
  };

  brosis = (event) => {
    this.setState({ brosis: event.target.value });
  };

  capitalize = (string) => {
    return string.charAt(0).toUpperCase() + string.slice(1);
  };
  handleheight = (event) => {
    this.setState({ height: event.target.value });
  };

  additems = (e) => {
    e.preventDefault();
    var uid = uuidv4();
    console.log(uid);
    const { openSnackbar } = this.props;
    const detail = this.props.location.detail;
    const inputs = {
      Name: this.capitalize(this.state.Name),
      age: this.state.age,
      profession: this.capitalize(this.state.profession),
      Location: this.capitalize(this.state.Location),
      birthplace: this.capitalize(this.state.birthplace),
      fname: this.capitalize(this.state.fname),
      mname: this.state.mname,
      nakshatra: this.capitalize(this.state.nakshatra),
      lagnam: this.capitalize(this.state.lagnam),
      neramandalam: this.state.neramandalam,
      rasi: this.state.rasi,
      kulam: this.capitalize(this.state.kulam),
      mothertTongue: this.capitalize(this.state.mothertTongue),
      rasiathipathi: this.state.rasiathipathi,
      Ayanamsa: this.state.Ayanamsa,
      degree: this.state.degree,
      gothiram: this.state.gothiram,
      Deergasam: this.state.Deergasam,
      address: this.state.address,
      inam: this.state.inam,
      height: this.state.height,
      sunrise: this.state.sunrise,
      sunset: this.state.sunset,
      yogam: this.state.yogam,
      karnam: this.state.karnam,
      phnnum: this.state.phnnum,
      tamildate: this.state.tamildate,
      tamilmonth: this.state.tamilmonth,
      tamilyear: this.state.tamilyear,
      date: this.state.startDate.getDate(),
      Month: this.state.startDate.getMonth(),
      year: this.state.startDate.getFullYear(),
      hour: this.state.startDate.getHours(),
      min: this.state.startDate.getMinutes(),
      brosis: this.state.brosis,
      Gender: this.state.gender,
      thidhi: this.state.thidhi,
      color: this.state.color,
      ayanam: this.state.ayanam,
      atchamsum: this.state.atchamsum,
      udayadinaligai: this.state.udayadinaligai,
      natchathirahorai: this.state.natchathirahorai,
      id: detail ? detail.id : uid,
      inputA: this.state.inputA,
      inputB: this.state.inputB,
      inputC: this.state.inputC,
      inputD: this.state.inputD,
      inputE: this.state.inputE,
      inputF: this.state.inputF,
      inputG: this.state.inputG,
      inputH: this.state.inputH,
      inputI: this.state.inputI,
      inputJ: this.state.inputJ,
      inputK: this.state.inputK,
      inputL: this.state.inputL,
      input2A: this.state.input2A,
      input2B: this.state.input2B,
      input2C: this.state.input2C,
      input2D: this.state.input2D,
      input2E: this.state.input2E,
      input2F: this.state.input2F,
      input2G: this.state.input2G,
      input2H: this.state.input2H,
      input2I: this.state.input2I,
      input2J: this.state.input2J,
      input2K: this.state.input2K,
      input2L: this.state.input2L,
    };

    console.log("uid is", uid);
    console.log("inputs is", inputs);

    if (detail) {
      console.log("id is", detail.id);
      console.log("detail is", detail);
      //   const detail = this.props.location.detail
      //   const updates = {};
      //   updates['/Details/' + detail.id] = inputs;
      //   firebase.database().ref().update(updates)
      firebase.database().ref(`Details/${detail.id}`).set(inputs);
      openSnackbar("edited");
      this.props.history.push("/Home");
    } else {
      firebase.database().ref(`Details/${uid}`).set(inputs);
      this.setState({ isLoading: true });
      openSnackbar("Information Added sucessfully");
      this.props.history.push("/Home");
    }
  };

  componentDidMount() {
    const detail = this.props.location.detail;
    //console.log(detail)
    if (detail) {
      this.setState({ Name: detail.Name });
      this.setState({ age: detail.age });
      this.setState({ profession: detail.profession });
      this.setState({ Location: detail.Location });
      this.setState({ fname: detail.Location });
      this.setState({ nakshatra: detail.nakshatra });
      this.setState({ date: detail.startDate });
      this.setState({ thidhi: detail.thidhi });
      this.setState({ gender: detail.Gender });
      this.setState({ rasi: detail.rasi });
      this.setState({ rasi: detail.rasiathipathi });
      this.setState({ color: detail.color });
      this.setState({ inputA: detail.inputA });
      this.setState({ inputB: detail.inputB });
      this.setState({ inputC: detail.inputC });
      this.setState({ inputD: detail.inputD });
      this.setState({ inputE: detail.inputE });
      this.setState({ inputF: detail.inputF });
      this.setState({ inputG: detail.inputG });
      this.setState({ inputH: detail.inputH });
      this.setState({ inputI: detail.inputI });
      this.setState({ inputJ: detail.inputJ });
      this.setState({ inputK: detail.inputK });
      this.setState({ inputL: detail.inputL });
      this.setState({ input2A: detail.input2A });
      this.setState({ input2B: detail.input2B });
      this.setState({ input2C: detail.input2C });
      this.setState({ input2D: detail.input2D });
      this.setState({ input2E: detail.input2E });
      this.setState({ input2F: detail.input2F });
      this.setState({ input2G: detail.input2G });
      this.setState({ input2H: detail.input2H });
      this.setState({ input2I: detail.input2I });
      this.setState({ input2J: detail.input2J });
      this.setState({ input2K: detail.input2K });
      this.setState({ input2L: detail.input2L });
    }
  }
  render() {
    return (
      <Form>
        <Button
          style={{
            float: "right",
            marginRight: 20,
            borderRadius: 25,
            padding: "10px 20px 15px 20px",
            backgroundColor: "#e2e6dd",
            color: "black",
            borderColor: "#e2e6dd",
          }}
          onClick={this.exit}
        >
          x
        </Button>
        <header
          className="Addnew"
          style={{ textAlign: "center", fontSize: 30 }}
        >
          <div style={{ marginTop: 20 }}>Add new details</div>
        </header>
        <div>
          <div style={{ marginTop: 30 }}>
            <div style={{ display: "flex", flexDirection: "row" }}>
              <div style={{ width: "20vw", marginLeft: 20 }}>
                <Label>பெயர்</Label>
                <Input
                  type="text"
                  onChange={this.handleName}
                  value={this.state.Name}
                ></Input>
              </div>
              <div style={{ width: "8vw", marginLeft: 10 }}>
                <Label>வயது</Label>
                <Input
                  type="number"
                  onChange={this.handleAge}
                  value={this.state.age}
                ></Input>
              </div>
              <div style={{ width: "20vw", marginLeft: 10 }}>
                <Label>தொழில்</Label>
                <Input
                  type="text"
                  onChange={this.handleProfession}
                  value={this.state.profession}
                ></Input>
              </div>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "row  ",
                marginLeft: 10,
                marginTop: 20,
              }}
            >
              <div style={{ width: "21vw", marginLeft: 10 }}>
                <Label>அப்பா பெயர்</Label>
                <Input
                  type="text"
                  onChange={this.handlefname}
                  value={this.state.fname}
                ></Input>
              </div>
              <div style={{ width: "20vw", marginLeft: 10 }}>
                <Label>அம்மா பெயர்</Label>
                <Input
                  type="text"
                  onChange={this.handlemname}
                  value={this.state.mname}
                ></Input>
              </div>
              <div style={{ marginTop: 32, marginLeft: 10 }}>
                <ButtonDropdown
                  isOpen={this.state.dropdownOpen1}
                  toggle={this.gendertoggle}
                >
                  <DropdownToggle caret>
                    {this.state.gender === null ? "Gender" : this.state.gender}
                  </DropdownToggle>
                  <DropdownMenu onClick={this.handlegenderChange}>
                    <DropdownItem>Male</DropdownItem>
                    <DropdownItem>Female</DropdownItem>
                  </DropdownMenu>
                </ButtonDropdown>
              </div>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "row  ",
                marginLeft: 10,
                marginTop: 20,
              }}
            >
              <div style={{ width: "15vw", marginLeft: 10 }}>
                <Label>நக்ஷத்திரம்</Label>
                <Input
                  type="text"
                  onChange={this.handlenakshatra}
                  value={this.state.nakshatra}
                ></Input>
              </div>
              <div style={{ width: "15vw", marginLeft: 10 }}>
                <Label>லக்னம்</Label>
                <Input
                  type="text"
                  onChange={this.handlelagnam}
                  value={this.state.lagnam}
                ></Input>
              </div>
              <div
                style={{
                  marginLeft: 20,
                  display: "flex",
                  flexDirection: "column",
                }}
              >
                <Label> பிறந்த தேதி</Label>
                <DatePicker
                  selected={this.state.startDate}
                  onChange={this.handleDate}
                  name="startDate"
                  dateFormat="MM/dd/yyyy"
                  showTimeSelect
                  timeFormat="HH:mm"
                  timeIntervals={1}
                  timeCaption="time"
                />
              </div>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "row  ",
                marginLeft: 10,
                marginTop: 20,
              }}
            >
              <div style={{ width: "13vw", marginLeft: 10 }}>
                <Label>நிறம்</Label>
                <Input
                  type="text"
                  onChange={this.color}
                  value={this.state.color}
                ></Input>
              </div>
              <div style={{ width: "15vw", marginLeft: 10 }}>
                <Label>திதி </Label>
                <Input
                  type="text"
                  onChange={this.thidhi}
                  value={this.state.thidhi}
                ></Input>
              </div>
              <div style={{ marginLeft: 20, marginTop: 32 }}>
                <ButtonDropdown
                  direction="right"
                  isOpen={this.state.dropdownOpen2}
                  toggle={this.rasitoggle}
                  style={{ width: 100 }}
                >
                  <DropdownToggle caret color="info">
                    {this.state.rasi === null ? "ராசி" : this.state.rasi}
                  </DropdownToggle>
                  <DropdownMenu onClick={this.handlerasiChange}>
                    <DropdownItem>மேஷம்</DropdownItem>
                    <DropdownItem>ரிஷபம்</DropdownItem>
                    <DropdownItem>மிதுனம்</DropdownItem>
                    <DropdownItem>கடகம்</DropdownItem>
                    <DropdownItem>சிம்மம்</DropdownItem>
                    <DropdownItem>கன்னி </DropdownItem>
                    <DropdownItem>துலாம் </DropdownItem>
                    <DropdownItem>விருச்சிகம்</DropdownItem>
                    <DropdownItem>தனுசு</DropdownItem>
                    <DropdownItem>மகரம்</DropdownItem>
                    <DropdownItem>கும்பம்</DropdownItem>
                    <DropdownItem>மீனம்</DropdownItem>
                  </DropdownMenu>
                </ButtonDropdown>
              </div>
              <div style={{ marginLeft: 20, marginTop: 32 }}>
                <ButtonDropdown
                  direction="right"
                  isOpen={this.state.dropdownOpen3}
                  toggle={this.rasiathipatitoggle}
                  style={{ width: 130 }}
                >
                  <DropdownToggle caret color="info">
                    {this.state.rasiathipathi === null ? (
                      <span style={{ fontSize: 13 }}>ராசி அதிபதி</span>
                    ) : (
                      this.state.rasiathipathi
                    )}
                  </DropdownToggle>
                  <DropdownMenu onClick={this.handlerasiathipathiChange}>
                    <DropdownItem>செவ்வாய்</DropdownItem>
                    <DropdownItem>சுக்கிரன்</DropdownItem>
                    <DropdownItem>புதன்</DropdownItem>
                    <DropdownItem>சந்திரன்</DropdownItem>
                    <DropdownItem>சூரியன்</DropdownItem>
                    <DropdownItem> குரு </DropdownItem>
                    <DropdownItem>சனி </DropdownItem>
                  </DropdownMenu>
                </ButtonDropdown>
              </div>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "row  ",
                marginLeft: 10,
                marginTop: 20,
              }}
            >
              <div style={{ width: "16vw", marginLeft: 10 }}>
                <Label>பிறந்த இடம்</Label>
                <Input
                  type="text"
                  onChange={this.birthplace}
                  value={this.state.birthplace}
                ></Input>
              </div>
              <div style={{ width: "16vw", marginLeft: 10 }}>
                <Label>தீர்காசம்</Label>
                <Input
                  type="text"
                  onChange={this.Deergasam}
                  value={this.state.Deergasam}
                ></Input>
              </div>
              <div style={{ width: "16vw", marginLeft: 10 }}>
                <Label>அயனாசம்</Label>
                <Input
                  type="text"
                  onChange={this.Ayanamsa}
                  value={this.state.Ayanamsa}
                ></Input>
              </div>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "row  ",
                marginLeft: 10,
                marginTop: 20,
              }}
            >
              <div style={{ width: "16vw", marginLeft: 10 }}>
                <Label>நேரமண்டலம்</Label>
                <Input
                  type="text"
                  onChange={this.neramandalam}
                  value={this.state.neramandalam}
                ></Input>
              </div>
              <div style={{ width: "16vw", marginLeft: 10 }}>
                <Label>குலம்</Label>
                <Input
                  type="text"
                  onChange={this.kulam}
                  value={this.state.kulam}
                ></Input>
              </div>
              <div style={{ width: "16vw", marginLeft: 10 }}>
                <Label>கோத்திரம்</Label>
                <Input
                  type="text"
                  onChange={this.gothiram}
                  value={this.state.gothiram}
                ></Input>
              </div>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "row  ",
                marginLeft: 10,
                marginTop: 20,
              }}
            >
              <div style={{ width: "16vw", marginLeft: 10 }}>
                <Label>தாய் மொழி</Label>
                <Input
                  type="text"
                  onChange={this.mothertTongue}
                  value={this.state.motheTongue}
                ></Input>
              </div>
              <div style={{ width: "16vw", marginLeft: 10 }}>
                <Label>பட்டம்</Label>
                <Input
                  type="text"
                  onChange={this.handledegree}
                  value={this.state.degree}
                ></Input>
              </div>
              <div style={{ width: "16vw", marginLeft: 10 }}>
                <Label>சகோதர-சகோதரிகள்</Label>
                <Input
                  type="text"
                  onChange={this.brosis}
                  value={this.state.brosis}
                ></Input>
              </div>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "row  ",
                marginLeft: 10,
                marginTop: 20,
              }}
            >
              <div style={{ width: "16vw", marginLeft: 10 }}>
                <Label>இனம்</Label>
                <Input
                  type="text"
                  onChange={this.inam}
                  value={this.state.inam}
                ></Input>
              </div>
              <div style={{ width: "16vw", marginLeft: 10 }}>
                <Label>உயரம்</Label>
                <Input
                  type="text"
                  onChange={this.handleheight}
                  value={this.state.height}
                ></Input>
              </div>
              <div style={{ width: "16vw", marginLeft: 10 }}>
                <Label>யோகம்</Label>
                <Input
                  type="text"
                  onChange={this.yogam}
                  value={this.state.yogam}
                ></Input>
              </div>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "row  ",
                marginLeft: 10,
                marginTop: 20,
              }}
            >
              <div style={{ width: "16vw", marginLeft: 10 }}>
                <Label>சூரிய உதயம்</Label>
                <Input
                  type="text"
                  onChange={this.sunrise}
                  value={this.state.sunrise}
                ></Input>
              </div>
              <div style={{ width: "16vw", marginLeft: 10 }}>
                <Label>சூரியஅஸ்தமனம்</Label>
                <Input
                  type="text"
                  onChange={this.sunset}
                  value={this.state.sunset}
                ></Input>
              </div>
              <div style={{ width: "16vw", marginLeft: 10 }}>
                <Label>கர்ணம்</Label>
                <Input
                  type="text"
                  onChange={this.karnam}
                  value={this.state.karnam}
                ></Input>
              </div>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "row  ",
                marginLeft: 10,
                marginTop: 20,
              }}
            >
              <div style={{ width: "16vw", marginLeft: 10 }}>
                <Label>தொலைபேசி எண்</Label>
                <Input
                  type="number"
                  onChange={this.phnnum}
                  value={this.state.phnnum}
                ></Input>
              </div>
              <div style={{ width: "16vw", marginLeft: 10 }}>
                <Label>நக்ஷத்திரஹோரை</Label>
                <Input
                  type="text"
                  onChange={this.natchathirahorai}
                  value={this.state.natchathirahorai}
                ></Input>
              </div>
              <div style={{ width: "16vw", marginLeft: 10 }}>
                <Label>உதயாதிநாழிகை</Label>
                <Input
                  type="text"
                  onChange={this.udayadinaligai}
                  value={this.state.udayadinaligai}
                ></Input>
              </div>
            </div>
            <div
              style={{
                display: "flex",
                flexDirection: "row  ",
                marginLeft: 10,
                marginTop: 20,
              }}
            >
              <div style={{ width: "16vw", marginLeft: 10 }}>
                <Label>அயனம்</Label>
                <Input
                  type="text"
                  onChange={this.ayanam}
                  value={this.state.ayanam}
                ></Input>
              </div>
              <div style={{ width: "16vw", marginLeft: 10 }}>
                <Label>அட்சாம்சம்</Label>
                <Input
                  type="text"
                  onChange={this.atchamsum}
                  value={this.state.atchamsum}
                ></Input>
              </div>
              <div style={{ width: "15vw", marginLeft: 10 }}>
                <Label>இடம்</Label>
                <Input
                  type="text"
                  onChange={this.handleLocation}
                  value={this.state.Location}
                ></Input>
              </div>
            </div>

            <div
              style={{
                display: "flex",
                flexDirection: "column",
                marginLeft: 20,
                marginTop: 10,
              }}
            >
              <Label>Address</Label>
              <textarea
                style={{ width: "50vw", height: "20vh", borderWidth: 2 }}
                placeholder="Write here...."
                onChange={this.address}
                value={this.state.address}
              ></textarea>
            </div>
            <div
              style={{
                marginLeft: 20,
                marginTop: 32,
                display: "flex",
                flexDirection: "row",
              }}
            >
              <div style={{ marginTop: 5 }}> Tamil Calender-</div>
              <ButtonDropdown
                direction="right"
                isOpen={this.state.dropdownOpen5}
                toggle={this.tamildate}
                style={{ marginLeft: 20, width: 80 }}
              >
                <DropdownToggle caret color="info">
                  {this.state.tamildate === null ? (
                    <span style={{}}>Date</span>
                  ) : (
                    this.state.tamildate
                  )}
                </DropdownToggle>
                <DropdownMenu onClick={this.handletamildate}>
                  <DropdownItem>1</DropdownItem>
                  <DropdownItem>2 </DropdownItem>
                  <DropdownItem>3 </DropdownItem>
                  <DropdownItem>4 </DropdownItem>
                  <DropdownItem>5 </DropdownItem>
                  <DropdownItem>6 </DropdownItem>
                  <DropdownItem>7</DropdownItem>
                  <DropdownItem>8 </DropdownItem>
                  <DropdownItem>9</DropdownItem>
                  <DropdownItem>10</DropdownItem>
                  <DropdownItem>11 </DropdownItem>
                  <DropdownItem>12 </DropdownItem>
                  <DropdownItem>13 </DropdownItem>
                  <DropdownItem>14 </DropdownItem>
                  <DropdownItem>15 </DropdownItem>
                  <DropdownItem>16 </DropdownItem>
                  <DropdownItem>17 </DropdownItem>
                  <DropdownItem>18 </DropdownItem>
                  <DropdownItem>19 </DropdownItem>
                  <DropdownItem>20 </DropdownItem>
                  <DropdownItem>21 </DropdownItem>
                  <DropdownItem>22 </DropdownItem>
                  <DropdownItem>23 </DropdownItem>
                  <DropdownItem>24 </DropdownItem>
                  <DropdownItem>25 </DropdownItem>
                  <DropdownItem>26 </DropdownItem>
                  <DropdownItem>27 </DropdownItem>
                  <DropdownItem>28 </DropdownItem>
                  <DropdownItem>29 </DropdownItem>
                  <DropdownItem>30 </DropdownItem>
                  <DropdownItem>31 </DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>

              <ButtonDropdown
                direction="right"
                isOpen={this.state.dropdownOpen4}
                toggle={this.tamilmonth}
                style={{ width: 100, marginLeft: 20 }}
              >
                <DropdownToggle caret color="info">
                  {this.state.tamilmonth === null ? (
                    <span style={{}}>Tamilmonth</span>
                  ) : (
                    this.state.tamilmonth
                  )}
                </DropdownToggle>
                <DropdownMenu onClick={this.handletamilmonth}>
                  <DropdownItem>சித்திரை </DropdownItem>
                  <DropdownItem>வைகாசி </DropdownItem>
                  <DropdownItem>ஆனி </DropdownItem>
                  <DropdownItem>ஆடி </DropdownItem>
                  <DropdownItem>ஆவணி </DropdownItem>
                  <DropdownItem>புரட்டாசி </DropdownItem>
                  <DropdownItem>ஐப்பசி </DropdownItem>
                  <DropdownItem>கார்த்திகை</DropdownItem>
                  <DropdownItem>மார்கழி </DropdownItem>
                  <DropdownItem>தை</DropdownItem>
                  <DropdownItem>மாசி </DropdownItem>
                  <DropdownItem>பங்குனி </DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
              {/* <div style={{ width: "5vw", marginLeft: 40, }}>
                <Input type="text" onChange={this.tamilyear} value={this.state.tamilyear} placeholder="year"></Input>
              </div> */}
              <ButtonDropdown
                direction="right"
                isOpen={this.state.dropdownOpen6}
                toggle={this.tamilyear}
                style={{ width: 100, marginLeft: 40 }}
              >
                <DropdownToggle caret color="info">
                  {this.state.tamilyear === null ? (
                    <span style={{}}>Tamilyear</span>
                  ) : (
                    this.state.tamilyear
                  )}
                </DropdownToggle>
                <DropdownMenu onClick={this.handletamilyear}>
                  <DropdownItem>பிரபாவ(1987 - 1988) </DropdownItem>
                  <DropdownItem>விபவ(1988 - 1989) </DropdownItem>
                  <DropdownItem>சுக்ல(1989 - 1990) </DropdownItem>
                  <DropdownItem>பிரமோதூத(1990 - 1991) </DropdownItem>
                  <DropdownItem>பிரசோற்பத்தி(1991 - 1992) </DropdownItem>
                  <DropdownItem>ஆங்கிரச(1992 - 1993) </DropdownItem>
                  <DropdownItem>ஸ்ரிமுக்ஹா(1993 - 1994) </DropdownItem>
                  <DropdownItem>பாவ(1994 - 1995)</DropdownItem>
                  <DropdownItem>யுவ(1995 - 1996)</DropdownItem>
                  <DropdownItem>தாது(1996 - 1997)</DropdownItem>
                  <DropdownItem>ஈஸ்வர(1997 - 1998)</DropdownItem>
                  <DropdownItem>வேஹுதான்ய(1998 - 1999)</DropdownItem>
                  <DropdownItem>பிரமாதி(1999 - 2000)</DropdownItem>
                  <DropdownItem>விக்கிரம(2000 - 2001)</DropdownItem>
                  <DropdownItem>விஷு(2001 - 2002 )</DropdownItem>
                  <DropdownItem>சித்ரபானு(2002 - 2003 )</DropdownItem>
                  <DropdownItem>சுபானு(2003 - 2004)</DropdownItem>
                  <DropdownItem>தாரண(2004 - 2005 )</DropdownItem>
                  <DropdownItem>பார்த்திப2005 - 2006 )</DropdownItem>
                  <DropdownItem>விய(2006 - 2007)</DropdownItem>
                  <DropdownItem>சர்வஜித்(2007 - 2008)</DropdownItem>
                  <DropdownItem>சர்வதாரி(2008 - 2009) </DropdownItem>
                  <DropdownItem>விரோதி( 2009 - 2010)</DropdownItem>
                  <DropdownItem>விக்ருதி(2010 - 2011) </DropdownItem>
                  <DropdownItem>கர(2011 - 2012)</DropdownItem>
                  <DropdownItem>நந்தன(2012 - 2013) </DropdownItem>
                  <DropdownItem>விஜய(2013 - 2014)</DropdownItem>
                  <DropdownItem>ஜெயா(2014 - 2015)</DropdownItem>
                  <DropdownItem>மன்மத(2015 - 2016)</DropdownItem>
                  <DropdownItem>துன்முகி(2016 - 2017)</DropdownItem>
                  <DropdownItem>ஹேவிளம்பி(2017 - 2018) </DropdownItem>
                  <DropdownItem>விளம்பி(2018 - 2019)</DropdownItem>
                  <DropdownItem>விகாரி(2019 - 2020)</DropdownItem>
                  <DropdownItem>சார்வரி(2020 - 2021)</DropdownItem>
                  <DropdownItem>பலவா(2021 - 2022)</DropdownItem>
                  <DropdownItem>சுபக்ரித்(2022 - 2023)</DropdownItem>
                  <DropdownItem>சொபக்ரித்(2023 - 2024)</DropdownItem>
                  <DropdownItem>குரோதி(2024 - 2025)</DropdownItem>
                  <DropdownItem>விசுவாசுவ(2025 - 2026)</DropdownItem>
                  <DropdownItem>பரபாவ(2026 - 2027)</DropdownItem>
                  <DropdownItem>பிலவங்க(2027 - 2028)</DropdownItem>
                  <DropdownItem>கீழாக(2028 - 2029)</DropdownItem>.0
                  <DropdownItem>சாம்ய(2029 - 2030)</DropdownItem>
                  <DropdownItem>சாதாரண(2030 - 2031)</DropdownItem>
                  <DropdownItem>விரோதிக்ரித்து(2031 - 2032)</DropdownItem>
                  <DropdownItem>பரிதாபி(2032 - 2033)</DropdownItem>
                  <DropdownItem>பிரமாதிச(2033 - 2034)</DropdownItem>
                  <DropdownItem>ஆனந்த(2034 - 2035)</DropdownItem>
                  <DropdownItem>ராக்ஷச(2035 - 2036)</DropdownItem>
                  <DropdownItem>நல(2036 - 2037)</DropdownItem>
                  <DropdownItem>பிங்கள(2037 - 2038)</DropdownItem>
                  <DropdownItem>காளயுக்தி(2038 - 2039)</DropdownItem>
                  <DropdownItem>சித்தார்த்தி(2039 - 2040)</DropdownItem>
                  <DropdownItem>ராவ்திரி(2040 - 2041)</DropdownItem>
                  <DropdownItem>துன்மதி(2041 - 2042)</DropdownItem>
                  <DropdownItem>துந்துபி(2042 - 2043)</DropdownItem>
                  <DropdownItem>ருத்ரோத்காரி(2043 - 2044)</DropdownItem>
                  <DropdownItem>ரக்டக்ஷி(2044 - 2045)</DropdownItem>
                  <DropdownItem>குரோதன(2045 - 2046)</DropdownItem>
                  <DropdownItem>அக்ஷய(2046 - 2047)</DropdownItem>
                </DropdownMenu>
              </ButtonDropdown>
            </div>
          </div>
        </div>
        <div style={{ display: "flex", flexDirection: "row", marginTop: 30 }}>
          <div style={{ display: "flex", flexDirection: "column" }}>
            <div>
              {this.state.newinputs1.map((input1, idd) => (
                <div key={idd}>
                  <div style={{ display: "flex", flexDirection: "row" }}>
                    {input1.map((subinput, subId) => (
                      <div
                        key={subId}
                        style={{ width: "20%", marginLeft: 20, marginTop: 30 }}
                      >
                        <Label>Field {subinput} </Label>
                        <textarea
                          onChange={(event) =>
                            this.handlefields1(event, subinput)
                          }
                          value={this.state[`input${subinput}`]}
                          style={{ height: 100, width: 100 }}
                        ></textarea>
                      </div>
                    ))}
                  </div>
                </div>
              ))}
            </div>
            <div>
              {this.state.newinputs2.map((input2, iddd) => (
                <div key={iddd}>
                  <div style={{ display: "flex", flexDirection: "row" }}>
                    {input2.map((subinput2, subId2) => (
                      <div
                        key={subId2}
                        style={{ width: "20%", marginLeft: 20, marginTop: 30 }}
                      >
                        <Label>Field {subinput2} </Label>
                        <textarea
                          onChange={(event) =>
                            this.handlefields2(event, subinput2)
                          }
                          value={this.state[`input2${subinput2}`]}
                          style={{ width: 100, height: 100 }}
                        ></textarea>
                      </div>
                    ))}
                  </div>
                </div>
              ))}
              <Button
                type="submit"
                color="info"
                style={{ marginLeft: "40%", marginTop: 40, width: 100 }}
                onClick={this.additems}
                disabled={!this.state.input2L}
              >
                {this.state.isLoading ? (
                  <Spinner animation="border" size="sm" />
                ) : (
                  <div>Sumbit</div>
                )}
              </Button>
            </div>
          </div>

          <div
            style={{
              backgroundColor: "#dcdcdcd",
              width: "50vw",
              height: "110vh",
            }}
          >
            <Tableoutput
              // Name={this.state.Name}
              // age={this.state.age}
              // Location={this.state.Location}
              // profession={this.state.profession}
              // Gender={this.state.gender}
              // fname={this.state.fname}
              newinputs1={this.state}
              newinputs2={this.state}
            />
          </div>
        </div>
      </Form>
    );
  }
}
export default withSnackbar(Addnew);
