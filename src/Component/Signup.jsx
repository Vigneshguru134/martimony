import React, { Component } from "react";
import { Link } from 'react-router-dom'
import { Spinner, Label, Input, FormGroup, Form, Button, } from "reactstrap"
import firebase from '../Firebase'
import image from '../assets/bgimage1.jpg'
import { withSnackbar } from 'react-simple-snackbar'

import { v4 as uuidv4 } from "uuid";

class Signup extends Component {
  state = {
    email: '',
    password: '',
    isLoading: false,

  }
  handleEmail = event => {
    this.setState({ email: event.target.value })
  }
  handlePassword = event => {
    this.setState({ password: event.target.value })
  }
  handleSubmit = (e) => {
    const { openSnackbar } = this.props
    var id = uuidv4()
    e.preventDefault()
    this.setState({ isLoading: true })
    firebase.auth().createUserWithEmailAndPassword(this.state.email, this.state.password).then((res) => {
      console.log('condition true', res)
      openSnackbar('Accont created successfully')
      this.setState({ isLoading: false })
      firebase.auth().signInWithEmailAndPassword(this.state.email, this.state.password).then((res) => {
        localStorage.setItem("uid", res.user.uid);
        this.props.history.push("/Home");
        //openSnackbar(res.message)
        firebase.database().ref(`Users/${id}`).set({
          email: this.state.email,
          password: this.state.password,
          Role: 'User',
          uuid: id
        })

      })
    }).catch((error) => {
      this.setState({ isLoading: false })
      console.log('condition false', error)
      openSnackbar(error.message)

    })
  }
  render() {
    return (
      <Form style={{ display: 'flex', flexDirection: 'row' }} onSubmit={this.handleSubmit}>
        <div>
          <img src={image} alt="" style={{ width: '75vw', height: '100vh' }} />
        </div>
        <div style={{ width: '20vw', marginLeft: 50 }}>
          <div style={{ marginTop: 50, textAlign: "center", fontSize: 20 }}><strong>Anandha Mugurtham</strong></div>
          <div style={{ marginTop: 50, textAlign: "center", fontSize: 20 }}><strong>Signup</strong></div>
          <div style={{ marginTop: 30 }}>
            <FormGroup>
              <Label >Email address</Label>
              <Input type="email" className="form-control" id="exampleInputEmail1" placeholder="Enter email" onChange={this.handleEmail} ></Input>
            </FormGroup>
            <FormGroup>
              <Label >Password</Label>
              <Input type="password" className="form-control" id="exampleInputPassword1" placeholder="Password" onChange={(e) => this.handlePassword(e)}></Input>
            </FormGroup>
            <Button type="submit" style={{ marginLeft: 90, width: 100 }} className="btn btn-info" >
              {this.state.isLoading ?
                <Spinner animation="border" className="isLoading" size="sm" />
                :
                <div>Submit</div>}
            </Button>
            <div style={{ marginTop: 25, textAlign: "center" }}><Link to="/">Registered User ! Click here to login</Link></div>
          </div>
        </div>
      </Form>

    );
  }
}
export default withSnackbar(Signup)