import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Spinner, Label, Input, FormGroup, Form, Button, Card, CardImg, CardText, CardTitle } from "reactstrap";
import firebase from "../Firebase";
import image from "../assets/bgimage1.jpg"
import { withSnackbar } from 'react-simple-snackbar'
import image1 from "../assets/logo.png"
import image2 from "../assets/slogo.png"
import image3 from "../assets/file.png"

class Login extends Component {

  state = {
    email: "",
    password: "",
    isLoading: false,
    cardinfo: [
      { title: "Register", body: "as", content: "Register for free to gain access from our genuine profiles to find your ideal match " },
      { title: "Search", body: "as", content: "Multiple types of search to filter your ideal match to pull from millions of profiles " },
      { title: "View", body: "as", content: "View the full information of the profiles including contact and horoscope " },
      { title: "Contact", body: "as", content: "Contact the profile for next steps.. Good luck for your future endeavours!! " }
    ]
  };
  handleEmail = (event) => {
    this.setState({ email: event.target.value });
  };
  handlePassword = (event) => {
    this.setState({ password: event.target.value });
  };
  handleSubmit = (event) => {
    const { openSnackbar } = this.props
    event.preventDefault();
    this.setState({ isLoading: true })

    firebase
      .auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .then((res) => {
        console.log(res);
        //console.log(role)
        // console.log(res.message);
        // console.log(localStorage.getItem("uid"));
        firebase.database().ref('Users').on('value', (snap) => {
          //console.log(snap.val())
          var valu = Object.values(snap.val())
          const obj = valu.find(({ email }) => email === this.state.email)
          const Role = obj.Role
          localStorage.setItem('Role', Role)
          this.props.history.push('/Home')
        })

        this.setState({ isLoading: false })
        localStorage.setItem("uid", res.user.uid);
        openSnackbar("Welcome")
      }).catch((error) => {
        this.setState({ isLoading: false })
        openSnackbar(error.message)
        console.log(error)
      })
  };

  componentDidMount() {
    if (localStorage.getItem("uid")) {
      this.props.history.push("/Home");
    }
  }



  render() {
    return (
      <div>
        <Form
          style={{ display: "flex", flexDirection: "row" }}
          onSubmit={this.handleSubmit}
        >
          <div>
            <img
              src={image}
              alt=""
              style={{ width: "75vw", height: "100vh" }}

            />
          </div>
          <div style={{ width: "20vw", marginLeft: 30 }}>
            <div style={{ marginTop: 50, textAlign: "center", fontSize: 20 }}>
              <strong>Anandha Mugurtham</strong>
            </div>
            <div style={{ marginTop: 30, textAlign: "center", fontSize: 20 }}>
              <strong>Login</strong>
            </div>
            <div style={{ marginTop: 30 }}>
              <FormGroup>
                <Label>Email address</Label>
                <Input
                  type="email"
                  style={{}}
                  className="form-control"
                  id="exampleInputEmail1"
                  placeholder="Enter email"
                  onChange={this.handleEmail}
                  required
                ></Input>
              </FormGroup>
              <FormGroup>
                <Label>Password</Label>
                <Input
                  type="password"
                  className="form-control"
                  id="exampleInputPassword1"
                  placeholder="Password"
                  onChange={this.handlePassword}
                  required
                ></Input>
              </FormGroup>
              <Button
                type="submit"
                style={{ width: 100, marginLeft: 90 }}
                className="btn btn-info"
              >
                {this.state.isLoading ? (
                  <Spinner animation="border" size="sm" />
                ) : (
                    <div>Submit</div>
                  )}
              </Button>
              <br />

              <div style={{ textAlign: 'center', marginTop: 25 }}>
                <Link to="/Signup">New users!Click here to signup</Link>
              </div>
            </div>
          </div>
          <div>
          </div>
        </Form>

        <Card style={{ marginLeft: 50, width: "92%", marginTop: 70, display: 'flex', alignItems: 'center', padding: '30px 0px  30px 0px', }}>

          <CardImg src={image1} style={{ width: '60vw' }} />
          <CardText style={{ fontFamily: "serif", padding: '20px 50px 0px 50px' }}>Varangal a Mugurtham product is a matrimonial destination proudly founded since 2014 with one simple objective - to provide a superior collaborative matchmaking experience by expanding the opportunities available to meet potential life partners. Since then we have created a world renowned service that has touched the lives of millions of people all over the world . We have, however, never rested on our laurels. Software division providing cutting edge business solutions to Matrimony Industry. An innovator in teleMatrimony and analytics. Partner focused on quality and growth. </CardText>

        </Card>
        <div style={{ display: "flex", flexDirection: "row", padding: "30px 0px 30px 0px" }}>
          {this.state.cardinfo.map((info, iid) => (
            <div key={iid}>
              <Card style={{ width: "20vw", marginLeft: 50, backgroundColor: "", height: "50vh" }}>
                <CardImg style={{ height: 40, width: 40, marginLeft: 10, marginTop: 10 }} src={image2} />
                <CardTitle style={{ textAlign: "center", fontSize: 25, }}>{info.title}</CardTitle>
                <div style={{ height: 4, width: '7vw', backgroundColor: "#04ff00", marginLeft: 84 }}></div>
                <CardImg style={{ width: 50, height: 50, marginTop: 30, marginLeft: "40%" }} src={image3} />
                <CardText style={{ padding: 10, fontFamily: "Times, serif", marginLeft: 20, marginTop: 15 }}><strong>{info.content}</strong></CardText>
              </Card>
            </div>
          ))}

        </div>
        <div style={{ height: 50 }}></div>


      </div>


    );
  }
}

export default withSnackbar(Login)