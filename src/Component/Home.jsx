import React, { Component } from 'react'
import { Navbar, Button, Input, Card, CardBody, Spinner, Modal, ModalHeader, ModalBody, ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import firebase from '../Firebase'
import image1 from '../assets/Man.png'
import image2 from '../assets/Women.png'
import match from '../config/matchData'
// import Tableout from './Tableoutput'
// import Addnew from './Addnew    '



export class Home extends Component {
    state = {
        logout: '',
        details: [],
        editbtn: false,
        isloading: false,
        search: '',
        searchResults: [],
        alert: true,
        showModal: false,
        info: '',
        showFilterModal: false,
        showMatchModel: false,
        dropdownOpen: false,
        dropdownOpen1: false,
    }
    handleview = (detail) => {
        console.log('view', detail)
        this.setState({
            showModal: !this.state.showModal
        })
        this.setState({ info: detail })


    }

    logout = () => {
        firebase.auth().signOut().then(() => {
            localStorage.clear()
            this.props.history.push("/")
            console.log("signout sucessfully")
        }).catch((error) =>
            console.log(error))
    }
    addnew = () => {
        this.props.history.push('/Addnew')
    }
    componentDidMount() {
       console.log("match data",Object.keys(match))
        // firebase.database().ref('Details/3a05b8ec-4d6e-4458-9b60-f973b7ea313c').set({ demo: "arun" })
        if (localStorage.getItem("uid") === null) {
            this.props.history.push("/");
        }
        console.log(this.state.details)
        this.setState({ isloading: true })

        firebase.database().ref('Details').on('value', (snap) => {

            this.setState({ isloading: false })
            if (snap.val() !== null) {
                const key = Object.values(snap.val())
                this.setState({ details: key })
                //console.log(snap.val())
            }
            else { }
        })
    }

    handleFilter = () => {

        this.setState({
            showFilterModal: !this.state.showFilterModal
        })
        //this.setState({ info: detail })
    }
    handleMatch = () => {
        this.setState({
            showMatchModel: !this.state.showMatchModel
        })
    }

    handleSearch = (event) => {
        const query = event.target.value;
        const details = Object.assign([], this.state.details)
        let queryText = query.toLowerCase().replace(/,|\.|-/g, ' ');

        const queryWords = queryText.split(' ').filter(w => !!w.trim().length);
        let searchedItems = [];
        details.forEach(profile => {
            if (profile.Name.toLowerCase().indexOf(queryWords) > -1) {
                searchedItems.push(profile)
            }
        })
        this.setState({
            searchResults: searchedItems,
        }, );
        this.setState({
            search: event.target.value
        })
        console.log(searchedItems)
        /* 
          info=(name)=>{
              ("this is"+name)
          }

          det=(input)=>{
              var name=('name');
              input(name)
          }
          det (name)
        
        */
        // console.log('seraced result', searchedItems)
        // if(this.state.search.length===0){
        //     "show all cards"
        // }
        // else if (this.state.search.lenght!==0&&searchResults.length!==0) {
        //     "show particular profile"
        // }
        // else if (searchResults.lenght ===0) {
        //     "No profile found"
        // }
        console.log('search', this.state.search)
        console.log('this.search items', this.state.searchResults)
    };


    deletedet = (id) => {
        firebase.database().ref(`Details/${id}`).remove()
        //this.setState({ alert:  })
    }
    edit = (detail, editbtn) => {
        this.setState({ editbtn: editbtn });
        console.log(detail)
        this.props.history.push({
            pathname: '/Addnew',
            detail: detail,
        })

    }


    handleGender = () => {
        this.setState({
            dropdownOpen: !this.state.dropdownOpen
        })
    }
    handleName = () => {
        this.setState({
            dropdownOpen1: !this.state.dropdownOpen1
        })
    }
    print = () => {
        // var content = document.getElementById('printarea');
        // var pri = document.getElementById('ifmcontentstoprint').contentWindow;
        // pri.document.open();
        // pri.document.write(content.innerHTML);
        // pri.document.close();
        // pri.focus();
        // pri.print();
        window.print();
    }
    render() {
        const Role = localStorage.getItem('Role')
        const info = this.state.info
        const namedet = this.state.details.map((detail, iuid) => (
            <div key={iuid}>
                {detail.Name}
            </div>
        ))

        return (
            <div>
                <Navbar color="light"  >
                    <div style={{ marginLeft: 10, marginTop: 0, fontSize: 25 }}> Anandha Mugurutam</div>
                    {Role === "Admin" ? <Button style={{ color: 'black', backgroundColor: "#ffffff", justifyContent: "right", alignItems: 'right', }} onClick={this.addnew}>Add New</Button>
                        : ""}
                    <Button style={{ color: "black", backgroundColor: "#ffffff", }} onClick={this.handleMatch}>Match</Button>
                    <Input type="text" placeholder="Search" style={{ width: '40vw', }} onChange={this.handleSearch}></Input>

                    {/* {this.state.details.filter((detail, iddi) => (
                        <div key={iddi}></div>
                        <div>{this.state.filter===  }</div>
                    ))} */}
                    <Button style={{ color: "black", backgroundColor: "#ffffff", marginLeft: 0 }} onClick={this.handleFilter}>Filters</Button>
                    <Button style={{ color: 'black', backgroundColor: "#ffffff", marginRight: 50 }} onClick={this.logout}>Logout</Button>
                </Navbar>
                <Modal isOpen={this.state.showFilterModal} toggle={this.handleFilter} size="lg" >
                    <ModalHeader  ><div style={{ display: 'flex', flexDirection: "row" }}>
                        <div>Apply Filters</div>
                        <div> <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.handleGender} style={{ marginLeft: 50 }}  >
                            <DropdownToggle caret>
                                Gender
                 </DropdownToggle>
                            <DropdownMenu onClick={(event) => this.handleName(event)} >
                                <DropdownItem>Male</DropdownItem>
                                <DropdownItem >Female</DropdownItem>
                            </DropdownMenu>
                        </ButtonDropdown></div></div></ModalHeader>
                    <ModalBody >
                        Age:
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.showMatchModel} toggle={this.handleMatch} size="lg" >
                    <ModalHeader  ><div style={{ display: 'flex', flexDirection: "row" }}>
                        <div>Apply Filters</div>
                        <div> <ButtonDropdown isOpen={this.state.dropdownOpen1} toggle={this.handleName} style={{ marginLeft: 50 }}  >
                            <DropdownToggle caret>
                                Name
                 </DropdownToggle>
                            <DropdownMenu onClick={(event) => this.handleName(event)} >
                                <DropdownItem>{namedet}</DropdownItem>
                                {/* <DropdownItem>male</DropdownItem> */}

                            </DropdownMenu>
                        </ButtonDropdown></div></div></ModalHeader>
                    <ModalBody >
                        Age:
                    </ModalBody>
                </Modal>
                <Modal isOpen={this.state.showModal} toggle={this.handleview} size="lg" >
                    <ModalHeader  ><div style={{ display: 'flex', flexDirection: "row" }}>
                        <div>Profile Details</div>
                        <div style={{ marginLeft: 50 }}><Button onClick={this.print}>print</Button></div></div></ModalHeader>

                    <ModalBody id='printarea' className="print">
                        {/* {this.state.details.map((info, idid) => (
                            info.Name
                        ))} */}
                        {/* <iframe id="ifmcontentstoprint" style={{
                            height: "0vh",
                            width: "0vw",
                            color: '#ffff'
                        }}></iframe> */}
                        <div >
                            <div style={{ display: "flex", flexDirection: "row" }}>
                                <div >
                                    <div> <strong >பெயர்</strong>:<span style={{ marginLeft: 10 }}>{info.Name}</span><br /></div>
                                    <div> <strong >நிறம்</strong>:<span style={{ marginLeft: 10 }}>{info.color}</span><br /></div>
                                    <div> <strong> வயது</strong>:<span style={{ marginLeft: 10 }}>{info.age}</span><br /></div>
                                    <div><strong>தொழில்</strong>:<span style={{ marginLeft: 10 }}>{info.profession}</span><br /></div>
                                    <div> <strong>இடம்</strong>:<span style={{ marginLeft: 10 }}>{info.Location} </span> <br /></div>
                                    <div> <strong>ராசி</strong>:<span style={{ marginLeft: 10 }}>{info.rasi} </span> <br /></div>
                                    <div> <strong>ராசி அதிபதி</strong>:<span style={{ marginLeft: 10 }}>{info.rasiathipathi}</span>  <br /></div>
                                    <div> <strong>நக்ஷத்திரம்</strong>:<span style={{ marginLeft: 10 }}>{info.nakshatra}  </span><br /></div>
                                    <div> <strong>உயரம்</strong>:<span style={{ marginLeft: 10 }}>{info.height}</span>  <br /></div>
                                    <div> <strong>பிறந்த இடம்</strong>:<span style={{ marginLeft: 10 }}>{info.birthplace} </span> <br /></div>
                                    <div> <strong>கோத்திரம்</strong>:<span style={{ marginLeft: 10 }}>{info.gothiram} </span> <br /></div>
                                    <div> <strong>தாய் மொழி</strong>:<span style={{ marginLeft: 10 }}>{info.mothertTongue} </span> <br /></div>
                                    <div> <strong>யோகம்</strong>:<span style={{ marginLeft: 10 }}>{info.yogam} </span> <br /></div>
                                    <div> <strong>அப்பா பெயர்</strong>:<span style={{ marginLeft: 10 }}>{info.fname} </span> <br /></div>
                                    <div> <strong>அம்மா பெயர்</strong>:<span style={{ marginLeft: 10 }}>{info.mname} </span> <br /></div>
                                    <div> <strong>பிறந்தநேரம் </strong>:<span style={{ marginLeft: 10 }}> {`${info.hour}:${info.min}:00`}(H-M-S)  </span><br /></div>

                                    <div> <strong>முகவரி</strong>:<span style={{ marginLeft: 10 }}>{info.address} </span><br /></div>

                                </div>
                                <div style={{ marginLeft: 200 }}>
                                    <div> <strong>தீர்காசம்</strong>:<span style={{ marginLeft: 10 }}>{info.Deergasam} </span> <br /></div>
                                    <div> <strong>சகோதர சகோதரிகள்</strong>:<span style={{ marginLeft: 10 }}>{info.brosis}  </span><br /></div>
                                    <div> <strong>குலம்</strong>:<span style={{ marginLeft: 10 }}>{info.kulam}</span>  <br /></div>
                                    <div> <strong>இனம்</strong>:<span style={{ marginLeft: 10 }}>{info.inam}  </span><br /></div>
                                    <div> <strong>தமிழ் தேதி</strong>:<span style={{ marginLeft: 10 }}>{`${info.tamildate} - ${info.tamilmonth} - ${info.tamilyear}`}(DD-MM-YYYY) </span> <br /></div>
                                    <div> <strong>பிறந்த தேதி</strong>:<span style={{ marginLeft: 10 }}>{`${info.date} - ${info.Month} - ${info.year}`}(DD-MM-YYYY) </span> <br /></div>
                                    <div> <strong>நேரமண்டலம்</strong>:<span style={{ marginLeft: 10 }}>{info.neramandalam} </span> <br /></div>
                                    <div> <strong>தொலைபேசி எண்</strong>:<span style={{ marginLeft: 10 }}>{info.phnnum} </span> <br /></div>
                                    <div> <strong>சூரிய உதயம்</strong>:<span style={{ marginLeft: 10 }}>{info.sunrise} </span> <br /></div>
                                    <div> <strong>சூரிய அஸ்தமனம்</strong>:<span style={{ marginLeft: 10 }}>{info.sunset} </span> <br /></div>
                                    <div> <strong>லக்னம்</strong>:<span style={{ marginLeft: 10 }}>{info.lagnam} </span> <br /></div>
                                    <div> <strong>கர்ணம்</strong>:<span style={{ marginLeft: 10 }}>{info.karnam} </span> <br /></div>
                                    <div> <strong>பட்டம்</strong>:<span style={{ marginLeft: 10 }}>{info.degree} </span> <br /></div>
                                    <div> <strong>அயனாசம்</strong>:<span style={{ marginLeft: 10 }}>{info.Ayanamsa} </span> <br /></div>
                                    <div> <strong>திதி</strong>:<span style={{ marginLeft: 10 }}>{info.thidhi} </span> <br /></div>
                                    <div> <strong>நக்ஷத்திர ஹோரை </strong>:<span style={{ marginLeft: 10 }}>{info.natchathirahorai} </span> <br /></div>
                                    <div> <strong> உதயாதி நாழிகை</strong>:<span style={{ marginLeft: 10 }}>{info.udayadinaligai} </span> <br /></div>

                                </div>

                            </div>


                            <div style={{ display: "flex", flexDirection: 'row', marginTop: 50 }}>
                                <div style={{ marginTop: 20, marginLeft: 50 }}>
                                    <div style={{ display: "flex", flexDirection: "row" }}>
                                        <div className="squbox">
                                            {info.inputA}
                                        </div>
                                        <div className="squbox">
                                            {info.inputB}
                                        </div>
                                        <div className="squbox">
                                            {info.inputC}
                                        </div>
                                        <div className="squbox">
                                            {info.inputD}
                                        </div>
                                    </div>
                                    <div style={{ display: "flex", flexDirection: "row" }}>
                                        <div style={{}}>
                                            <div className="squbox">
                                                {info.inputE}
                                            </div>
                                            <div className="squbox">
                                                {info.inputF}
                                            </div>
                                        </div>
                                        <div style={{ marginLeft: 130 }}>
                                            <div className="squbox">
                                                {info.inputL}
                                            </div>
                                            <div className="squbox">
                                                {info.inputK}
                                            </div>
                                        </div>
                                    </div>
                                    <div style={{ display: "flex", flexDirection: "row" }}>
                                        <div className="squbox" >
                                            {info.inputG}
                                        </div>
                                        <div className="squbox">
                                            {info.inputH}
                                        </div>
                                        <div className="squbox" >
                                            {info.inputI}
                                        </div>
                                        <div className="squbox" >
                                            {info.inputJ}
                                        </div>
                                    </div>
                                </div>
                                <div style={{ marginTop: 20, marginLeft: 100 }}>
                                    <div style={{ display: "flex", flexDirection: "row" }}>
                                        <div className="squbox" >
                                            {info.input2A}
                                        </div>
                                        <div className="squbox" >
                                            {info.input2B}
                                        </div>
                                        <div className="squbox" >
                                            {info.input2C}
                                        </div>
                                        <div className="squbox" >
                                            {info.input2D}
                                        </div>
                                    </div>
                                    <div style={{ display: "flex", flexDirection: "row" }}>
                                        <div style={{}}>
                                            <div className="squbox" >
                                                {info.input2E}
                                            </div>
                                            <div className="squbox" >
                                                {info.input2F}
                                            </div>
                                        </div>
                                        <div style={{ marginLeft: 130 }}>
                                            <div className="squbox">
                                                {info.input2K}
                                            </div>
                                            <div className="squbox" >
                                                {info.input2L}
                                            </div>
                                        </div>
                                    </div>
                                    <div style={{ display: "flex", flexDirection: "row" }}>
                                        <div className="squbox">
                                            {info.input2G}
                                        </div>
                                        <div className="squbox" >
                                            {info.input2H}
                                        </div>
                                        <div className="squbox">
                                            {info.input2I}
                                        </div>
                                        <div className="squbox" >
                                            {info.input2J}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {/* // <Tableout /> */}
                        </div>
                    </ModalBody>
                </Modal>

                {
                    this.state.isloading ? <div style={{ textAlign: "center", marginTop: 100 }}  >
                        <Spinner animation="border" size="50" /></div>
                        :
                        <div>
                            {this.state.details.length === 0 ?
                                <div style={{ textAlign: "center", fontSize: 20, marginTop: 100, fontFamily: "serif" }}>
                                    No profile found ,To add profile click <strong>Add New</strong> button</div>
                                : <div>
                                    {this.state.search.length === 0 ?

                                        this.state.details.map((detail, uid) => (
                                            <div key={uid}>
                                                <div >
                                                    {

                                                        <Card
                                                            style={{
                                                                width: '80vw',
                                                                marginLeft: 100,
                                                                borderRadius: 8,
                                                                height: "23vh",
                                                                display: "flex",
                                                                padding: '0px 0px',
                                                                flexDirection: 'row',
                                                                marginTop: 40,
                                                                boxShadow: "7px 5px 10px 10px #f0f7f2 "
                                                            }}>
                                                            <img style={{
                                                                height: '18vh',
                                                                borderRadius: '8px 0px 0px 8px',
                                                                objectFit: "contain",
                                                                width: 114,
                                                                marginTop: 20

                                                            }}
                                                                src={detail.Gender === "Male" ? image1 : image2}
                                                                alt="" />

                                                            <CardBody style={{ marginLeft: 20, display: 'flex', flexDirection: 'column' }}>
                                                                <div style={{ color: 'blue', fontSize: 20, }}>
                                                                    {detail.Name},{detail.age}<div style={{ justifyContent: "right", }} className="float-right">
                                                                        <div><Button style={{ height: 35 }} onClick={() => this.handleview(detail)}>View</Button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div style={{ marginTop: 7 }} >
                                                                    {detail.Location}
                                                                    <span style={{ justifyContent: "right", }} className="float-right">
                                                                        {Role === "Admin" ? <Button style={{ height: 35 }} onClick={() => this.edit(detail, this.state.editbtn)}>Edit</Button> : ""}
                                                                    </span>

                                                                </div>
                                                                <div style={{ marginTop: 5 }}>{detail.profession}<span style={{ justifyContent: "right", }} className="float-right">
                                                                    {Role === "Admin" ? <Button style={{ height: 35 }} onClick={() => this.deletedet(detail.id)} color="danger"> Delete </Button> : ""}
                                                                </span></div>

                                                            </CardBody>
                                                        </Card>
                                                    } </div>
                                            </div>
                                        ))
                                        : <div>{this.state.search.lenght !== 0 && this.state.searchResults.length !== 0 ?
                                            <div>

                                                {this.state.searchResults.map((info, idid) => (
                                                    <div key={idid}>
                                                        <Card
                                                            style={{
                                                                width: '80vw',
                                                                marginLeft: 100,
                                                                borderRadius: 8,
                                                                height: "23vh",
                                                                display: "flex",
                                                                padding: '0px 0px',
                                                                flexDirection: 'row',
                                                                marginTop: 10,
                                                                boxShadow: "7px 5px 10px 10px #f0f7f2 "
                                                            }}>
                                                            <img style={{
                                                                height: '18vh',
                                                                borderRadius: '8px 0px 0px 8px',
                                                                objectFit: "contain",
                                                                width: 114,
                                                                marginTop: 20

                                                            }}
                                                                src={info.Gender === "Male" ? image1 : image2}
                                                                alt="" />

                                                            <CardBody style={{ marginLeft: 20, display: 'flex', flexDirection: 'column' }}>
                                                                <span style={{ color: 'blue', fontSize: 20, }}>
                                                                    {info.Name},{info.age}
                                                                    <span style={{ justifyContent: "right", }} className="float-right">
                                                                        {Role === "Admin" ? <Button style={{ height: 35 }} onClick={() => this.edit(info, this.state.editbtn)}>Edit</Button> : ""}
                                                                    </span>
                                                                </span>
                                                                <div style={{ marginTop: 10 }} >
                                                                    {info.Location}
                                                                    <span style={{ justifyContent: "right", }} className="float-right">
                                                                        {Role === "Admin" ? <Button style={{ height: 35 }} onClick={(id) => this.deletedet(info, id)}> Delete </Button> : ""}
                                                                    </span>
                                                                </div>
                                                                <div style={{ marginTop: 5 }}>{info.profession}</div>

                                                            </CardBody>
                                                        </Card>
                                                    </div>)
                                                )}
                                            </div>
                                            :
                                            <div>{this.state.searchResults.length === 0 ?
                                                <div style={{ textAlign: "center", marginTop: 50, fontSize: 25 }} >No Profile Found</div>
                                                :
                                                "Empty string"}</div>}</div>}</div>
                            }</div>

                }

                {
                    /*<Alert onclick={this.deletedet}>csaa</Alert>*\}
                {/* <Card
                    style={{
                        width: '80vw',
                        marginLeft: 100,
                        borderRadius: 8,
                        height: "18vh",
                        display: "flex",
                        padding: '0px 0px',
                        flexDirection: 'row',
                        marginTop: 20,
                        boxShadow: "7px 5px 10px 10px #f0f7f2 "
                    }}>
                    <img style={{
                        height: '18vh',
                        borderRadius: '8px 0px 0px 8px',
                        objectFit: "contain",
                        width: 114,
                    }}
                        src={image1} />
                    <CardTitle style={{ marginLeft: 20, marginTop: 10, display: 'flex', flexDirection: 'column' }}>
                        <span style={{ color: 'blue', fontSize: 20, }}>Vidhya U </span>
                        <span style={{ marginTop: 10 }} >6'2 160.10cm,Hindu,Roman Catholic,Tamil,</span>
                        <span style={{ marginTop: 5 }}>Doctor&Sychologist,Singh</span>
                    </CardTitle>

                    <CardBody> </CardBody>
                </Card> */}
                {/* <Card
                    style={{
                        width: '80vw',
                        marginLeft: 100,
                        borderRadius: 8,
                        height: "18vh",
                        display: "flex",
                        flexDirection: 'row',
                        marginTop: 20,
                        boxShadow: "7px 5px 10px 10px #f0f7f2 ",
                        padding: '0px 0px'
                    }}>
                    <img
                        style={{
                            height: '18vh',
                            borderRadius: '8px 0px 0px 8px',
                            objectFit: "contain",
                            width: 114
                        }} src={image2} />
                    <CardTitle style={{ marginLeft: 20, marginTop: 10, display: 'flex', flexDirection: 'column' }}>
                        <span style={{ color: 'blue', fontSize: 20, }}>Dhanya S N</span>
                        <span style={{ marginTop: 10, }} >5'5 165.10cm,christian,Roman Catholic,Malayalam,</span>
                        <span style={{ marginTop: 5 }}>Medical&Healthcare,Patel</span>
                    </CardTitle>

                    <CardBody> </CardBody>
                </Card> */}

            </div >
        )
    }
}

export default Home
