import firebase from 'firebase'

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
  apiKey: "AIzaSyDZpek2-igfqIxeUMgQ-wDO-7wMrEbUJFY",
  authDomain: "martimony-fffc5.firebaseapp.com",
  databaseURL: "https://martimony-fffc5.firebaseio.com",
  projectId: "martimony-fffc5",
  storageBucket: "martimony-fffc5.appspot.com",
  messagingSenderId: "54169559012",
  appId: "1:54169559012:web:7cf68cd61e175fe893b1f1",
  measurementId: "G-5P453T8DXP"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

export default firebase;